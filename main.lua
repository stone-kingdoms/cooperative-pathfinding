local Grid = require('jumper.grid')
local army = require('army')
local resumeableastar = require('resumeableastar')

GROUND_TILE_BLACK_INDEX = 10 -- sprite index for "black" tiles
GROUND_TILE_WHITE_INDEX = 11 -- sprite index for "white" tiles
WALL_TILE_INDEX = 12 -- sprite index for wall tiles
MOVE_TIMING = 0.15 -- time (in seconds) that it takes for a turn to execute 
PIXEL_SCALE = 128 -- pixel size of a tile
WALL_PROBABILITY = 0.2 -- probability that a tile will be a wall in the random field generator
ARMY_SIZE = 100 -- number of critters in the randomly generated army
GRID_SIZE = 32 -- width and height of a random generated field

local waiting_state, moving_state

local unit_quad = love.graphics.newQuad(0,0,1,1,1,1)

local total_t = 0

local game = {}

waiting_state = {
	update = function(dt)
		if love.keyboard.isDown('space') then
			local start_time = love.timer.getTime()
			game.army:updateMoves()
			print(math.floor((love.timer.getTime() - start_time)*1000), "ms")
			game.moving_t = 0
			game.state = moving_state

		end

	end
}

moving_state = {
	update = function(dt)
--		total_t = total_t + 0.025
		game.moving_t = game.moving_t + dt / MOVE_TIMING
		if game.moving_t >= 1 then
			game.state = waiting_state
			game.moving_t = 1
		end
--		love.graphics.captureScreenshot(string.format('%0.1f.png', total_t * 8))
	end
}

local function image_to_array(filename, cols, rows, cell_width, cell_height)
	local image = love.graphics.newImage(filename)
	local my_canvas = love.graphics.newCanvas(cols * cell_width, rows * cell_height)
	my_canvas:renderTo(function() love.graphics.draw(image) end)
	local subimages = {}
	for y = 0, rows - 1 do
		for x = 0, cols - 1 do
			local i = y * cols + x + 1
			subimages[i] = my_canvas:newImageData(0, 1, x * cell_width, y * cell_height, cell_width, cell_height)
		end
	end
	return love.graphics.newArrayImage(subimages)
end


function shuffle(t)
	for i = 1, #t do
		local j = love.math.random(i, #t)
		t[i], t[j] = t[j], t[i]
	end
	return t
end

local function randomField()
	local grid_table = {}
	local critters = {}
	local destinations = {}
	local cell_count = 0

	for y = 1,GRID_SIZE do
		local grid_row = {}
		for x = 1, GRID_SIZE do
			if love.math.random() < WALL_PROBABILITY then
				grid_row[#grid_row+1] = 1
			else
				grid_row[#grid_row+1] = 0
				cell_count = cell_count + 1
				-- reservoir sample for sources and destinations
				if cell_count <= ARMY_SIZE then
					critters[cell_count] = {x=x, y=y}
					destinations[cell_count] = {x=x, y=y}
				else
					local new_critter_index = love.math.random(1,cell_count)
					if new_critter_index <= ARMY_SIZE then
						critters[new_critter_index] = {x=x, y=y}
					end
					local new_destination_index = love.math.random(1,cell_count)
					if new_destination_index <= ARMY_SIZE then
						destinations[new_destination_index] = {x=x, y=y}
					end
				end
			end
		end
		grid_table[#grid_table+1] = grid_row
	end
	shuffle(destinations)

	local start_time = love.timer.getTime()
	game.grid = Grid(grid_table)
	game.army = army.Army(game.grid)
	shuffle(critters)
	for k,critter_loc in ipairs(critters) do
		local critter = game.army:addCritter(game.grid:getNodeAt(critter_loc.x, critter_loc.y), game.grid:getNodeAt(destinations[k].x, destinations[k].y), 0, true, false)
	end
	print(love.timer.getTime() - start_time)
end

local function two_crowds()
	local grid_table = {
		{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
		{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
		{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
		{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
		{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
		{1,1,1,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,1,1,1},
		{1,1,1,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,1,1,1},
		{1,1,1,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,1,1,1},
		{1,1,1,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,1,1,1},
		{1,1,1,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,1,1,1},
		{1,1,1,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,1,1,1},
		{1,1,1,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,1,1,1},
		{0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0},
		{0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0},
		{0,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,0},
		{0,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,0},
		{0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
	}
	

	game.grid = Grid(grid_table)
	game.army = army.Army(game.grid)
	for i = 1,4 do
		for j = 1,4 do
			local point_a = game.grid:getNodeAt(i + 5, j + 7)
			local point_b = game.grid:getNodeAt(i + 15, j + 7)
			game.army:addCritter(point_a, point_b, 0, true, false)
			game.army:addCritter(point_b, point_a, 0, true, false)
		end
	end
	for i = 1,10 do
		for j = 1,2 do
			local point_a = game.grid:getNodeAt(i + 7, j + 1)
			local point_b = game.grid:getNodeAt(i + 7, j + 15)
			game.army:addCritter(point_a, point_b, 1, true, false)
			game.army:addCritter(point_b, point_a, 1, true, false)
		end
	end
end


function love.load()
	game.atlas = image_to_array('sprites.png', 3, 4, PIXEL_SCALE, PIXEL_SCALE)
	game.state = waiting_state
	-- randomField()
	two_crowds()
	game.moving_t = 1
--	love.graphics.captureScreenshot(string.format('%0.1f.png', total_t))
end



function love.update(dt)
	game.state.update(dt)
end

function love.draw()
	local grid_width, grid_height = game.grid:getWidth(), game.grid:getHeight()
	local window_width, window_height = love.graphics.getDimensions()
	local width_zoom, height_zoom = window_width / (grid_width + 1), window_height / (grid_height + 1) -- + 1 here to give a little bit of border so it doesn't look like it might continue
	local true_zoom = math.min(width_zoom, height_zoom)
	love.graphics.scale(true_zoom)
	love.graphics.translate((window_width / true_zoom - grid_width) / 2 - 1 , (window_height / true_zoom - grid_height) / 2 - 1)
	for y = 1, game.grid:getHeight() do
		for x = 1, game.grid:getWidth() do
			local layer
			if game.grid:isWalkableAt(x, y, 0) then
				if (x + y) % 2 == 1 then
					layer = GROUND_TILE_WHITE_INDEX
				else
					layer = GROUND_TILE_BLACK_INDEX
				end
			else
				layer = WALL_TILE_INDEX
			end
			love.graphics.drawLayer(game.atlas, layer, unit_quad, x, y)
		end
	end
	for _,critter in ipairs(game.army.critters) do
		local lerped_x = critter.location:getX() * game.moving_t + critter.previous_location:getX() * (1 - game.moving_t)
		local lerped_y = critter.location:getY() * game.moving_t + critter.previous_location:getY() * (1 - game.moving_t)
		love.graphics.drawLayer(game.atlas, critter:facing(), unit_quad, lerped_x, lerped_y)
		-- Draw agent priority values
		love.graphics.setColor(0,0,0)
		love.graphics.print(math.floor(critter.priority), lerped_x, lerped_y,nil, 0.025)
		love.graphics.setColor(1,1,1)
	end
end
