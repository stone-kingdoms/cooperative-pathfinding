local Heap = require('jumper.core.bheap')
local ras = require('resumeableastar')
local astaropennode = require('astaropennode')

SEARCH_DEPTH_WINDOW = 16

local TemporalGrid_index = {}
local TemporalGrid_meta = {__index = TemporalGrid_index}

--- Creates a Temporal Grid.
-- @class function
-- @treturn TemporalGrid a new temporal grid
local function newTemporalGrid()
	local self = setmetatable({}, TemporalGrid_meta)
	return self
end

--- the temporal grid setup is so you have a singular object for each pair of timestep and node.
-- this makes it so I can use node references directly instead of having to come up with some other storage technique.
-- I store one of these in the army so I can reuse nodes in the grid across the entire army, reducing churn.  

--- The Temporal Grid stores both spatial and temporal locations.
-- @type TemporalGrid
local TemporalGrid = setmetatable(
	{
		new = newTemporalGrid
	}, {__call = function(_, ...) return newTemporalGrid(...) end})

--- Collect (and if it doesn't exist yet) a singular location in the temporal grid.
-- @class function
-- @tparam Node node a node from a jumper grid
-- @tparam int t the number of timesteps in the future to look
-- @treturn table a temporal grid node, guaranteed to be the same as others with the same inputs
function TemporalGrid_index:getNode(node, t)
	if not self[t] then self[t] = {} end
	if not self[t][node] then self[t][node] = {node = node, t = t} end
	return self[t][node]
end

local army_index = {}
local army_meta = {__index = army_index}

--- Creates an Army.
-- @class function
-- @tparam Grid a Jumper grid
-- @treturn an army
local function newArmy(grid)
	local self = setmetatable({}, army_meta)
	self.grid = grid
	self.critters = {}
	self.claims = {}
	self.tick = 0
	self.cycle_time = SEARCH_DEPTH_WINDOW
	self.temporal_grid = TemporalGrid()
	return self
end

--- the Army class creates a cooperative pathfinding setup for a group of critters.
-- @type Army
local Army = setmetatable(
	{
		new = newArmy
	}, {__call = function(_, ...) return newArmy(...) end})

local critter_index = {}
local critter_meta = {__index = critter_index}

--- Initializes a critter for an army.
-- @class function
-- @tparam Army army the army that it will be created in
-- @tparam Node location the starting location of the critter
-- @tparam Node destination the location the critter wants to get to
-- @tparam int|string|func walkable a value or function that describes whether a cell is considered walkable
-- @tparam bool diagonal true if diagonal movement is allowed; false otherwise
-- @tparam bool tunnel true if, during diagonal movement, moving between two diagonally adjacent non-walkable cells is allowed
-- @treturn Critter the critter that is created
local function newCritter(army, location, destination, walkable, diagonal, tunnel)
	if not destination then destination = location end
	local self = setmetatable({
		location = location,
		previous_location = location,
		army = army,
		path = {},
		walkable = walkable,
		diagonal = diagonal,
		tunnel = tunnel
	}, critter_meta)
	self:setDestination(destination)
	return self
end

--- a Critter is a member of the Army
-- @type Critter
local Critter = setmetatable(
	{
		new = newCritter
	}, {__call = function(_, ...) return newCritter(...) end})

--- Create a critter in the army.
-- @class function
-- @tparam Node location the place the critter starts living
-- @tparam Node destination 
-- @treturn table a temporal grid node, guaranteed to be the same as others with the same inputs
function army_index:addCritter(location, destination, walkable, diagonal, tunnel)
	if not destination then destination = location end
	local critter = Critter(self, location, destination, walkable, diagonal, tunnel)
	self.critters[#self.critters + 1] = critter
    critter.priority = 0
	return critter
end

--- Update all the critters in the army to advance to their next move state
-- @class function
function army_index:updateMoves()
	for _,critter in ipairs(self.critters) do
        if #critter.path == self.cycle_time/4 then
            -- repath every so often
            local colisionless = false
            if critter.next_is_colissionless  then
                critter.next_is_colissionless = false
                colisionless = true
            end
            critter:findPath(colisionless)
        end
		critter:advance()
        critter:aggregatePriority()
        if critter.destination == critter.location then
            -- Agents which are at their destination should path last,
            -- otherwise they will never let other units pass through them
            critter.priority = -100
        end
	end
    for _,critter in ipairs(self.critters) do
        critter:finishPriority()
    end
	table.remove(self.claims, 1)
	self.tick = self.tick + 1
	if self.tick > self.cycle_time then
		self.tick = 1
	end
    local function compare(a,b)
        return a.priority > b.priority
    end      
	if self.tick == self.cycle_time/2 then
        table.sort(self.critters, compare)
    end
end

--- Find all the neighbors that a critter can reach starting from a particular point in spacetime without crashing into anybody else
-- @class function
-- @tparam TemporalNode temporal_location the current location in spacetime
-- @tparam Critter critter the critter, used to set pathfinding requirements
-- @tparam Boolean collisionless whether to ignore other units
function army_index:getUnclaimedNeighbors(temporal_location, critter, colisionless)
	local location, t = temporal_location.node, temporal_location.t
	if t == self.cycle_time * 2 then return {} end
	local permanent_neighbors = self.grid:getNeighbours(location, critter.walkable, critter.diagonal, critter.tunnel)
	local unclaimed_neighbors = {}
	if not self.claims[t] then self.claims[t] = {} end
	if not self.claims[t + 1] then self.claims[t + 1] = {} end
	for _, neighbor in ipairs(permanent_neighbors) do
		local next_occupied = self.claims[t + 1][neighbor]
		local direct_crash = self.claims[t][neighbor] and self.claims[t + 1][location] == self.claims[t][neighbor]
		local t_bone = false
		local diagonal_move = location:getX() ~= neighbor:getX() and location:getY() ~= neighbor:getY()
		if diagonal_move then
			local corner_a = self.grid:getNodeAt(location:getX(), neighbor:getY())
			local corner_b = self.grid:getNodeAt(neighbor:getX(), location:getY())
			local ab_collide = self.claims[t][corner_a] and self.claims[t][corner_a] == self.claims[t + 1][corner_b]
			local ba_collide = self.claims[t][corner_b] and self.claims[t][corner_b] == self.claims[t + 1][corner_a]
			t_bone = ab_collide or ba_collide
		end
        if colisionless then
            direct_crash = false
            t_bone = false
            next_occupied = false
        end
		if not (next_occupied or direct_crash or t_bone) then
			unclaimed_neighbors[#unclaimed_neighbors+1] = self.temporal_grid:getNode(neighbor, t + 1)
		end
	end
	if not self.claims[t + 1][location] or colisionless then
		unclaimed_neighbors[#unclaimed_neighbors+1] = self.temporal_grid:getNode(location, t + 1)
	end
	return unclaimed_neighbors
end

--- Delete a critter from the army.
-- @class function
-- @tparam Critter target the critter to delete
function army_index:deleteCritter(target)
	target:clearClaims() -- stop taking up spacetime
	for i, critter in ipairs(self.critters) do
		if critter == target then
			if i ~= #self.critters then
				self.critters[i] = self.critters[#self.critters] -- move the last critter into the gap that would be left by this one
				self.critters[i]:findPath() -- make sure the moved critter doesn't run low on path, if (for instance) it's moving from "just about to repath" to "won't repath until later"
			end
			self.critters[#self.critters] = nil -- delete the second reference to the previously-last critter
			break
		end
	end
end

IDLE_SPRITE_INDEX = 5
EAST_SPRITE_OFFSET = 1
SOUTH_SPRITE_OFFSET = 3

--- Calculate the facing of a critter based on current and previous locations
-- @class function
-- @treturn int A number from 1 to 9 describing the direction of facing
function critter_index:facing()
	local dx = self.location:getX() - self.previous_location:getX()
	local dy = self.location:getY() - self.previous_location:getY()
	return IDLE_SPRITE_INDEX + EAST_SPRITE_OFFSET * dx + SOUTH_SPRITE_OFFSET * dy
end

--- Update a critter's location
-- @class function
-- @tparam Node the new location of the critter
function critter_index:updatePosition(location)
	self.previous_location, self.location = self.location, location
end


function critter_index:aggregatePriority()
    local priority = 0
    if self.army.claims[1] and self.army.claims[1][self.location] then
        if self.army.claims[1][self.location] ~= self then
            self.army.claims[1][self.location].priority = self.army.claims[1][self.location].priority + 2
        else
            self.army.claims[1][self.location].priority = self.army.claims[1][self.location].priority - 2
            if self.army.claims[2] and self.army.claims[2][self.location] == self then
                self.priority = self.priority - 2
            end
        end
    end
end


function critter_index:finishPriority()
    local priority = 0
    -- If the the next location we'll be at has an agent which isn't us
    local next_location = self.path[1]
    local next_agent = self.army.claims[1] and self.army.claims[1][next_location]
    if next_agent and next_agent ~= self then
        -- Check if HIS next location (t+1) is OUR next location at (t+2)
        if next_agent.path[1] == self.path[2] then
            -- Assign same priority
            if self.priority < next_agent.priority then
                self.priority = next_agent.priority - 1
            else
                next_agent.priority = self.priority + 1
            end
        end
    end
end

--- Move the critter one step along its path
-- @class function
function critter_index:advance()
    if self.path[2] then
        table.remove(self.path, 1)
        self:updatePosition(self.path[1])
    else
        -- No path found, we're falling back to regular A*
        self:findPath(true)
        -- This and the next search window will be collisionless search
        self.next_is_colissionless = true
        self.previous_location, self.location = self.location, self.location
    end
end

--- Set a new place for the critter to go, and have it figure out its way there.
-- @class function
-- @tparam Node destination the place the critter should want to go
function critter_index:setDestination(destination)
	self.destination = destination
	self.heuristic_map = ras.ResumeableAStar(self.army.grid, self.destination, self.location, self.walkable, self.diagonal, self.tunnel)
    self:findPath()
end

--- Clear the claims of a critter on paths through spacetime
-- @class function
function critter_index:clearClaims()
	for k, location in ipairs(self.path) do
		if self.army.claims[k] then
		self.army.claims[k][location] = nil
		end
	end
end

--- Claim a path through spacetime
-- @class function
function critter_index:setClaims()
	for k, location in ipairs(self.path) do
		if not self.army.claims[k] then
			self.army.claims[k] = {}
		end
		self.army.claims[k][location] = self
	end
end

--- Figure out a way through spacetime to get closer to the destination
-- @class function
-- @tparam Boolean colisionless whether the search should ignore other units movements and the reservation table
function critter_index:findPath(colisionless)
	self:clearClaims()
	local open = Heap()
	local closed = {}
	local army = self.army
	local temporal_grid = self.army.temporal_grid
	local AStarOpenNode = astaropennode.AStarOpenNode
	local starting_node = AStarOpenNode(temporal_grid:getNode(self.location, 1), 0, self.heuristic_map:distance(self.location), nil)
	open:push(starting_node)
	while not open:empty() do
		local current = open:pop()
		if not closed[current.node] then
			closed[current.node] = current.predecessor -- I only need the predecessors; weighting is no longer an issue at this point.
			if current.node.t == self.army.cycle_time*2 then
				local current_node = current.node
				local backward_path = {}
				while current_node do
					backward_path[#backward_path+1] = current_node.node
					current_node = closed[current_node]
				end
				local forward_path = {}
				for k = 1,#backward_path do -- don't keep the starting point, the path only covers the future.
					forward_path[k] = backward_path[#backward_path + 1 - k]
				end
				self.path = forward_path
                if not colisionless then
				    self:setClaims()
                end
				return true
			end
                for _, next_location in ipairs(army:getUnclaimedNeighbors(current.node, self, colisionless)) do
                    if not closed[next_location] then
                        local edge_cost = 1
                        if next_location.node._x == self.destination._x and next_location.node._y == self.destination._y then
                            edge_cost = 0
                        end
                        local wait_cost_offset = 0
                        if next_location.node._x == current.node.node._x and next_location.node._y == current.node.node._y and
                            edge_cost == 1 then
                            wait_cost_offset = 0.0000000001
                        end
                        local diagonal_cost_offset = 0
                        if not (next_location.node._x == current.node.node._x or next_location.node._y ==
                            current.node.node._y) and edge_cost == 1 then
                            diagonal_cost_offset = 0.0000000001
                        end
                        local next_node = AStarOpenNode(next_location, current.cost + edge_cost + diagonal_cost_offset - wait_cost_offset, self.heuristic_map:distance(next_location.node), current.node)
                        open:push(next_node)
                    end
                end           
		end
	end
    return false
end


return {Army = Army, Critter = Critter}
