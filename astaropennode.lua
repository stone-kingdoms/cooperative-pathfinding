local AStarOpenNode_index = {}
local AStarOpenNode_meta = {__index = AStarOpenNode_index}

--- Compare this node to another, see which one's a better next candidate
-- @class function
-- @tparam AStarOpenNode other the other node to check.
-- @treturn bool true if this node is better to go for than another.

function AStarOpenNode_meta:__lt(other)
	return self.total < other.total or -- my total estimated cost is smaller
		self.total == other.total and self.heuristic < other.heuristic -- or if that's a tie, I'm closer to the finish
end

--- Initializes an open node for an A* pathfinding function
-- @class function
-- @tparam Node node  a node in the graph we're pathfinding on
-- @tparam number cost the total cost it took to arrive at this node
-- @tparam number heuristic a guess at the minimum cost to arrive at the destination
-- @tparam Node predecessor the node we reached this one from
-- @treturn AStarOpenNode an open node to put in an A* heap

local function newAStarOpenNode(node, cost, heuristic, predecessor)
	local self = setmetatable({node = node, cost = cost, heuristic = heuristic, total = cost + heuristic, predecessor = predecessor}, AStarOpenNode_meta)
	return self
end

--- a node for use in A* pathfinding
-- @type AStarOpenNode
local AStarOpenNode = setmetatable(
	{
		new = newAStarOpenNode
	}, {__call = function(_, ...) return newAStarOpenNode(...) end})

return {AStarOpenNode = AStarOpenNode}