# Cooperative Pathfinding
  
An implementation of David Silver's Cooperative Pathfinding algorithm (WHCA*) with dynamic priority.  
Does not support agents larger than 1 node at the moment.  
Utilizes [Love2D](https://love2d.org/) for visualisation purposes.  
Built upon [Jumper](https://github.com/Yonaba/Jumper)'s framework.  

### Research papers  
  
1. [Cooperative Pathfinding AI Wisdom, David Silver](https://www.davidsilver.uk/wp-content/uploads/2020/03/coop-path-AIWisdom.pdf)  
2. [Cooperative Pathfinding, David Silver](https://www.aaai.org/Library/AIIDE/2005/aiide05-020.php)  
  
### Prerequisites  

1. Install LÖVE 11.4 from the [official website](https://love2d.org/)
  
### How to run

1. Clone the repository
2. Open terminal or command line in the directory where `main.lua` is located
3. Run `love .` and press or hold space bar to simulate step-by-step.  
  
### License  
  
MIT License

### Attribution  
  
Thanks to Vörnicus for the initial implementation.