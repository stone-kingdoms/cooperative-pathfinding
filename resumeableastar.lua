local Heap = require('jumper.core.bheap')
local astaropennode = require('astaropennode')

local function reversed_ipairs_iter(t, i)
    i = i - 1
    if i ~= 0 then
        return i, t[i]
    end
end

local function reversed_ipairs(t)
    return reversed_ipairs_iter, t, #t + 1
end

local ResumeableAStar_index = {}
local ResumeableAStar_meta = {__index = ResumeableAStar_index}

-- @Vornicus: so here's the deal.  the existing pathing stuff in jumper actually adds
-- path info to the grid nodes that are actually attached to the grid.
-- This is *bad for us* because unlike jumper's standard stuff we need to have
-- several pathing maps around at once, one for each agent.
-- Worse we have to keep these around so we can resume the resumeable
-- pathfinding algorithm.
-- so we'll have to do this all ourselves and not count on the stuff that jumper itself does.

--- Creates a new resumeable A* instance with the given destination.
-- @class function
-- @tparam Grid grid The grid that the instance will navigate over
-- @tparam Node source The source node to start from (note it gets reversed later)
-- @tparam Node destination the first place to try to navigate to
-- @tparam int|string|func the function that determines whether a thing is walkable
-- @treturn ResumeableAStar the resumeable A* instance
local function newResumeableAStar(grid, source, destination, walkable, diagonal, tunnel)
	local self = setmetatable({
		grid = grid,
		source = source,
		walkable = walkable,
		diagonal = diagonal,
		tunnel = tunnel,
		closed = {},
		open = Heap()}, ResumeableAStar_meta)
	local source_tree_node = astaropennode.AStarOpenNode(source, 0, 0)
	self.open:push(source_tree_node)
	self:resume(destination)
	return self
end

--- Resume execution of the A* instance with a new destination.
-- @class function
-- @tparam Node destination the next place to try to navigate to
function ResumeableAStar_index:resume(destination)
	-- step 1: rejigger all open nodes to have the correct heuristic and total for the new destination
	local function distanceToDestination(other)
		return math.max(math.abs(destination:getX() - other:getX()), math.abs(destination:getY() - other:getY()))
	end
	for _,item in ipairs(self.open._heap) do
		item.heuristic = distanceToDestination(item.node)
		item.total = item.heuristic + item.cost
	end
	self.open:heapify() -- this also requires re-sorting so we correctly go towards the destination
	-- step 2: pull an item from the open nodes
	while not self.open:empty() do
		local current = self.open:pop()
		if not self.closed[current.node] then
			self.closed[current.node] = {cost = current.cost, predecessor = current.predecessor}
			-- step 3: put the neighbors into open
			for _, next_node in reversed_ipairs(self.grid:getNeighbours(current.node, self.walkable, self.diagonal, self.tunnel)) do -- TODO bring in the pathing data used by the actual object.
				if not self.closed[next_node] then
					self.open:push(astaropennode.AStarOpenNode(next_node, current.cost + 1, distanceToDestination(next_node), current.node))
				end
			end
		end
		-- step 4: check to see if I've consumed the destination
		if current.node == destination then
			return
		end
	end
end

--- Find the distance to a destination; if it doesn't exist, resume pathfinding to find it
-- @class function
-- @tparam Node destination the place to try to reach
-- @treturn Number the distance to the destination, or infinity if it's unreachable
function ResumeableAStar_index:distance(destination)
	if not self.closed[destination] then
		self:resume(destination)
	end
	return self.closed[destination] and self.closed[destination].cost or math.huge
end

--- the ResumeableAStar class is used to have an A* pathfinding instance that can use existing results to get new ones
-- @type ResumeableAStar
local ResumeableAStar = setmetatable(
	{
		new = newResumeableAStar
	}, {__call = function(_, ...) return newResumeableAStar(...) end})

return {ResumeableAStar = ResumeableAStar}